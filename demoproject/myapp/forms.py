from django import forms
from .models import students_records

class Stuform(forms.ModelForm):
    class Meta:
        model=students_records
        fields = "__all__"