from __future__ import unicode_literals
from django.db import models

class students_records(models.Model):
    roll_number= models.IntegerField()
    name=models.CharField(max_length=50)
    gender=models.CharField(max_length=50)
    date_of_birth=models.DateField(null=True)
    def __str__(self):
        return self.name
    class Meta:
        db_table="students_records"