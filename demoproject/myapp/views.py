from django.shortcuts import render
from rest_framework import viewsets
from .models import students_records
from .serializers import studentSerializer

class studentList(viewsets.ModelViewSet):
    queryset = students_records.objects.all()
    serializer_class = studentSerializer