from rest_framework import serializers
from .models import students_records

class studentSerializer(serializers.ModelSerializer):
    class Meta:
        model=students_records
        fields=('roll_number','name','gender','date_of_birth')